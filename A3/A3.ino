#include <MapleFreeRTOS900.h>
#include <HardwareTimer.h> //定时器
HardwareTimer timera(2);
#include <EEPROM.h>
#include "time_new.h" //时间戳
uint8 Enina = PB3; //打印中断引脚
uint8 Eninb = PB8; //编码器中断引脚
uint8 tpwm = PA7; //呼吸灯
uint8 test = PB11;
uint16 oldtime[3000]; //存储脉冲数
unsigned int addeeprom = 0x801F000; //距离eeprom存储位置
unsigned int diameeprom = 0x801F001; //编码器直径eeprom存储位置
unsigned int pulseprom = 0x801F002; //编码器脉冲数eeprom存储位置
String getserial = ""; //储存串口临时数据
unsigned int gettime[30];//存储串口解析后的数据
unsigned int geit[30];//串口输出储存
unsigned int xage, xmon, xday, xhour, xminute, xsecond; //adist是距离，negtime是负时间，xegtime是正时间
int p1, p2, t2, xegtime;
unsigned long nne; //当前时间戳
unsigned int hhxx, hhxx1, nf, nf1; //编码器计数
float pi = 3.1415926;
unsigned int engx, engtime, longs; //所需脉冲数,时间
uint16 adist, xdimax, xplusex;
int xxyz[11];
unsigned int xip = 0;
int peizhi = 0;
uint16 pptime[6];
void setup() {
  attachInterrupt(Eninb, Encodera, RISING); //编码器
  attachInterrupt(Enina, printzd, RISING); //打印中断
  timera.pause();
  timera.setPeriod(50000);//单位us
  timera.setChannel1Mode(TIMER_OUTPUT_COMPARE);//设定比较器1触发中断
  timera.setCompare(TIMER_CH1, 1); //设定比较器1的值为1,当计数器计数到1时触发比较器1
  timera.attachCompare1Interrupt(spend);
  timera.refresh();
  timera.resume();//  开始计数
  pinMode(Eninb, INPUT_PULLUP);
  pinMode(Enina, INPUT_PULLDOWN);
  Serial2.begin(57600);
  Serial1.begin(115200);
  Serial.begin(115200);
  EEPROM.read(addeeprom, &adist);
  EEPROM.read(diameeprom, &xdimax);
  EEPROM.read(pulseprom, &xplusex);
  settext(2, 11, xdimax);
  settext(2, 12, xplusex);
  settext(0, 20, adist);
  pinMode(tpwm, PWM);
  pinMode(test, OUTPUT);
  xTaskCreate(seal, "Task1", 512, NULL, tskIDLE_PRIORITY + 2, NULL);
  xTaskCreate(hxd, "Task3", 128, NULL, tskIDLE_PRIORITY + 2, NULL);
  xTaskCreate(setoldtime, "Task3", 512, NULL, tskIDLE_PRIORITY + 2, NULL);
  vTaskStartScheduler();
}
static void seal(void *pvParameters) {
  //绝对延迟20ms
  static portTickType xLastWakeTime;
  const portTickType xFrequency = pdMS_TO_TICKS(20);
  xLastWakeTime = xTaskGetTickCount();
  for (;;) {
    if (Serial1.available() > 0)
    {
      vTaskDelay(20);
      while (Serial1.available() > 0)
      {
        getserial += char(Serial1.read());
      }
      seria();
      getserial = ""; //清空串口缓存
    }
    vTaskDelayUntil( &xLastWakeTime, xFrequency );
  }
}
//呼吸灯
static void hxd(void *pvParameters) {
  for (;;) {
    static unsigned int brig = 0;
    static unsigned int fade = 500;
    pwmWrite(tpwm, brig);
    brig = brig + fade;
    if (brig <= 0 || brig >= 65500) {
      vTaskDelay(600);
      fade = -fade;
    }
    vTaskDelay(15);
  }
}
static void setoldtime(void *pvParameters) {
  static portTickType xLastWakeTime1;
  const portTickType xFrequency = pdMS_TO_TICKS(1000);
  for (;;) {
    unsigned int fe = 0, fx = 0;
    fe = timezz();
    fx = nne - fe;
    setprintime(fx);
    if (peizhi >= 2) {
      setjgq();
      peizhi = 0;
    }
    putnowtime();
    settext(0, 11, averspeed());
    setintl();
    static char qf = 0;
    qf = !qf;
    digitalWrite(test, qf);
    vTaskDelayUntil( &xLastWakeTime1, xFrequency );
  }
}
void loop() {
}
unsigned int timezz() {
  int timein = 0, pq = 0;
  unsigned int t = 0, y = 0, y1 = 0, z = 0, c = 0, l = 0;
  engx = (adist / ((xdimax * pi) / 10)) * xplusex; //计算需要脉冲数
  if (longs >= 2999) {
    longs = 1;
    xip = 1;
  }
  //开始填充还未填充满
  if (xip == 0) {
    for (int q = longs; q > 0; q--)
    {
      y++;
      timein = oldtime[q] + timein;
      pq = engx - timein;
      if (pq <= 0)
      {
        t = (y * 150) / 1000;
        q = 0;
      }
      else if (q == 1)
      {
        z = engx - timein;
        c = z / oldtime[longs];
        t = ((c + y) * 150) / 1000;
        q = 0;
      }
    }
  }
  //开始循环读取
  else if (xip == 1) {
    for (int q = longs; q > 0; q--)
    {
      y1++;
      timein = oldtime[q] + timein;
      pq = engx - timein;
      if (pq <= 0)
      {
        q = 0;
        t = (y1 * 150) / 1000;
      }
      if (q == 1)
      {
        for (int k = 2999; k > 0; k--)
        {
          l++;
          timein = oldtime[q] + timein;
          pq = engx - timein;
          if (pq <= 0)
          {
            q = 0;
            k = 0;
            t = ((y1 + l) * 150) / 1000;
          }
        }
      }
    }
  }
  return t;
}
//串口数据解析
void seria()
{
  switch (getserial[0]) {
    case 0xC1:
      for (int x = 0; x < 3000; x++) {
        oldtime[x] = 0;
      }
      longs = 1;
      xip = 0;
      return;
    case 0xEE:
      switch (getserial[1]) {
        case 0xF7:
          nne = getnowtime();
          return;
        case 0xB1:
          switch (getserial[4]) {
            case 0x00:
              switch (getserial[6]) {
                case 0x07:
                  adist = getdist();
                  settext(0, 20, adist);
                  return;

              }
            case 0x0D:
              switch (getserial[8]) {
                case 0x2D:
                  xegtime = -timeb();
                  settiamxx();
                  return;
                default:
                  xegtime = timea();
                  settiamxx();
                  return;
              }
            case 0x01:
              switch (getserial[6]) {
                case 0x09:
                  age();
                  return;
                case 0x0A:
                  mon();
                  return;
                case 0x0B:
                  day();
                  return;
                case 0x0C:
                  hour();
                  return;
                case 0x0D:
                  minute();
                  return;
                case 0x0E:
                  second();
                  return;
              }
            case 0x02:
              switch (getserial[6]) {
                case 0x09:
                  xdimax = dimax();
                  settext(2, 11, xdimax);
                  return;
                case 0x0A:
                  xplusex = plusex();
                  settext(2, 12, xplusex);
                  return;
              }
            default: return;
          }
        default: return;
      }
    default: return;
  }
}
void putnowtime() //发送获取时间命令
{
  unsigned char as[6] = {0xEE, 0x82, 0xFF, 0xFC, 0xFF, 0xFF};
  Serial1.write(as, 6);
}
//得到当前时间
unsigned long getnowtime()
{
  unsigned long nowtime;
  gettime[0] = BCDtoDec(getserial[2]) + 2000;
  gettime[1] = BCDtoDec(getserial[3]);
  gettime[2] = BCDtoDec(getserial[5]);
  gettime[3] = BCDtoDec(getserial[6]);
  gettime[4] = BCDtoDec(getserial[7]);
  gettime[5] = BCDtoDec(getserial[8]);
  nowtime = settime();
  getserial = ""; //清空串口缓存
  return nowtime;
}
unsigned long settime()  //通过年月日时分秒计算时间戳
{
  struct tm_new time_now;
  time_now.tm_year = gettime[0] - YEAR_BASE;    // adj date to 2009-08-10
  time_now.tm_mon  = gettime[1] - 1;
  time_now.tm_mday = gettime[2];
  time_now.tm_hour = gettime[3];
  time_now.tm_min = gettime[4];
  time_now.tm_sec = gettime[5];
  return mktime_new(&time_now) - 28800;
}
int * gettimes(unsigned long atime) //通过时间戳计算年月日时分秒
{
  struct tm_new time_now;
  static  int gettmiea[6]; //存储转换后的时间 gettime()函数使用
  gmtime_new(&time_now, atime + 28800); //提交时间戳
  gettmiea[0] = time_now.tm_year + 1900;
  gettmiea[1] = time_now.tm_mon + 1;
  gettmiea[2] = time_now.tm_mday;
  gettmiea[3] = time_now.tm_hour;
  gettmiea[4] = time_now.tm_min;
  gettmiea[5] = time_now.tm_sec;
  return gettmiea;
}
//计算时间戳使用方法
static const unsigned char mon_lengths[2][MONSPERYEAR] =
{
  {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
  {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
} ;

static const unsigned short year_lengths[2] =
{
  365,
  366
} ;

void gmtime_new(struct tm_new * res, const unsigned long timestamp)
{
  unsigned long days, rem;
  unsigned int y;
  unsigned int yleap;
  const unsigned char * ip;

  days = timestamp / SECSPERDAY; //得到天
  rem  = timestamp % SECSPERDAY; //得到天之后秒数
  res->tm_hour = (rem / SECSPERHOUR);
  rem %= SECSPERHOUR;
  res->tm_min =  (rem / SECSPERMIN);
  res->tm_sec =  (rem % SECSPERMIN);
  if ((res->tm_wday = ((EPOCH_WDAY + days) % DAYSPERWEEK)) < 0)
    res->tm_wday += DAYSPERWEEK;
  y = EPOCH_YEAR;
  for (;;)
  {
    yleap = isleap(y);
    if (days < year_lengths[yleap])
    {
      break;
    }
    y++;
    days -= year_lengths[yleap];
  }
  res->tm_year = y - YEAR_BASE;
  res->tm_yday = days;
  ip = mon_lengths[yleap];
  for (res->tm_mon = 0; days >= ip[res->tm_mon]; ++res->tm_mon)
  {
    days -= ip[res->tm_mon];
  }
  res->tm_mday = days + 1;
  res->tm_isdst = -1;
}
static const unsigned short _DAYS_BEFORE_MONTH[12] =
{0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334};

#define _ISLEAP(y) (((y) % 4) == 0 && (((y) % 100) != 0 || (((y)+1900) % 400) == 0))
#define _DAYS_IN_YEAR(year) (_ISLEAP(year) ? 366 : 365)

unsigned long mktime_new(struct tm_new * tim_p)
{
  unsigned long tim = 0;
  unsigned long days = 0;
  long year;
  tim += tim_p->tm_sec + (tim_p->tm_min * _SEC_IN_MINUTE) +
         (tim_p->tm_hour * _SEC_IN_HOUR);
  days += tim_p->tm_mday - 1;
  days += _DAYS_BEFORE_MONTH[tim_p->tm_mon];
  if (tim_p->tm_mon > 1 && _DAYS_IN_YEAR (tim_p->tm_year) == 366)
    days++;
  for (year = 70; year < tim_p->tm_year; year++)
    days += _DAYS_IN_YEAR (year);
  tim += (days * _SEC_IN_DAY);
  return tim;
}
unsigned int  BCDtoDec(int xy)   //BCD转换十进制
{
  int tmp, tmmp;
  tmp = xy;
  tmmp = ((tmp >> 4) & 0x0F) * 10 + (tmp & 0x0F);
  return tmmp;
}
unsigned int  DectoBCD(int xy)   //十进制转BCD 输出使用Serial.write
{
  int tmp;
  tmp = xy;
  return ( (tmp / 10) * 16 + tmp % 10 );
}
//写入EEPROM
void eepromwrite(int add, int x) {
  EEPROM.write(add, x);
}
//修正时间正数
unsigned int timea() {
  unsigned int i, num;
  i = getserial.length() - 13;
  for (unsigned int u = 0; u < i; u++)
  {
    geit[u] = (BCDtoDec(getserial[u + 8]) - 30);
  }
  getserial = ""; //清空串口缓存
  num = xx(i);
  return num;
}
//修正时间负数
unsigned int timeb() {
  unsigned int i, num;
  i = getserial.length() - 14;
  for (unsigned int u = 0; u < i; u++)
  {
    geit[u] = (BCDtoDec(getserial[u + 9]) - 30);
  }
  getserial = ""; //清空串口缓存
  num = xx(i);
  return num;
}
//距离设置
unsigned int getdist() {
  unsigned int i, num;
  i = getserial.length() - 13;
  for (unsigned int u = 0; u < i; u++)
  {
    geit[u] = (BCDtoDec(getserial[u + 8]) - 30);
  }
  getserial = ""; //清空串口缓存
  num = xx(i);
  eepromwrite(addeeprom, num); //写入eeprom
  return num;
}
//编码器直径
unsigned int dimax() {
  unsigned int i, num;
  i = getserial.length() - 13;
  for (unsigned int u = 0; u < i; u++)
  {
    geit[u] = (BCDtoDec(getserial[u + 8]) - 30);
  }
  getserial = ""; //清空串口缓存
  num = xx(i);
  eepromwrite(diameeprom, num); //写入eeprom
  return num;
}
//编码器脉冲数
unsigned int plusex() {
  unsigned int i, num;
  i = getserial.length() - 13;
  for (unsigned int u = 0; u < i; u++)
  {
    geit[u] = (BCDtoDec(getserial[u + 8]) - 30);
  }
  getserial = ""; //清空串口缓存
  num = xx(i);
  eepromwrite(pulseprom, num); //写入eeprom
  return num;
}
//设置年
void age() {
  unsigned int i, num;
  i = getserial.length() - 13;
  for (unsigned int u = 0; u < i; u++)
  {
    geit[u] = (BCDtoDec(getserial[u + 8]) - 30);
  }
  getserial = ""; //清空串口缓存
  num = xx(i);
  setage(num);
}
//设置月
void mon() {
  unsigned int i, num;
  i = getserial.length() - 13;
  for (unsigned int u = 0; u < i; u++)
  {
    geit[u] = (BCDtoDec(getserial[u + 8]) - 30);
  }
  getserial = ""; //清空串口缓存
  num = xx(i);
  setmon(num);
}
//日期
void day() {
  unsigned int i, num;
  i = getserial.length() - 13;
  for (unsigned int u = 0; u < i; u++)
  {
    geit[u] = (BCDtoDec(getserial[u + 8]) - 30);
  }
  getserial = ""; //清空串口缓存
  num = xx(i);
  setday(num);
}
//时
void hour() {
  unsigned int i, num;
  i = getserial.length() - 13;
  for (unsigned int u = 0; u < i; u++)
  {
    geit[u] = (BCDtoDec(getserial[u + 8]) - 30);
  }
  getserial = ""; //清空串口缓存
  num = xx(i);
  sethour(num);
}
//分钟
void minute() {
  unsigned int i, num;
  i = getserial.length() - 13;
  for (unsigned int u = 0; u < i; u++)
  {
    geit[u] = (BCDtoDec(getserial[u + 8]) - 30);
  }
  getserial = ""; //清空串口缓存
  num = xx(i);
  setminute(num);
}
//秒
void second() {
  unsigned int i, num;
  i = getserial.length() - 13;
  for (unsigned int u = 0; u < i; u++)
  {
    geit[u] = (BCDtoDec(getserial[u + 8]) - 30);
  }
  getserial = ""; //清空串口缓存
  num = xx(i);
  setsecond(num);
}
//数组变正整数
unsigned int xx(unsigned int ss) {
  unsigned int num;
  switch (ss) {
    case 1: num = geit[0];  return num;
    case 2: num = geit[0] * 10 + geit[1]; return num;
    case 3: num = geit[0] * 100 + geit[1] * 10 + geit[2]; return num;
    case 4: num = geit[0] * 1000 + geit[1] * 100 + geit[2] * 10 + geit[3]; return num;
    case 5: num = geit[0] * 10000 + geit[1] * 1000 + geit[2] * 100 + geit[3] * 10 + geit[4]; return num;
    default: return 0;
  }
}
//设置修正正负数时间
void settiamxx() {
  unsigned int axx[6]; //存储时间戳转换后了年月日时分秒
  unsigned char as[13] = {0xEE, 0x81, 0x48, 0x54, 0x17, 0x06, 0x03, 0x07, 0x17, 0xFF, 0xFC, 0xFF, 0xFF};
  int x;
  x = nne + xegtime;
  for (int i = 0; i < 6; i++) {
    axx[i] = gettimes(x)[i];
  }
  as[8] = DectoBCD((axx[0]) - 2000);
  as[7] = DectoBCD(axx[1]);
  as[5] = DectoBCD(axx[2]);
  as[4] = DectoBCD(axx[3]);
  as[3] = DectoBCD(axx[4]);
  as[2] = DectoBCD(axx[5]);
  Serial1.write(as, 13);
}
void setage(int y) {
  unsigned char as[13] = {0xEE, 0x81, 0x48, 0x54, 0x17, 0x06, 0x03, 0x07, 0x17, 0xFF, 0xFC, 0xFF, 0xFF};
  as[8] = DectoBCD(y - 2000);
  as[7] = DectoBCD(gettime[1]);
  as[5] = DectoBCD(gettime[2]);
  as[4] = DectoBCD(gettime[3]);
  as[3] = DectoBCD(gettime[4]);
  as[2] = DectoBCD(gettime[5]);
  Serial1.write(as, 13);
}
void setmon(int y) {
  unsigned char as[13] = {0xEE, 0x81, 0x48, 0x54, 0x17, 0x06, 0x03, 0x07, 0x17, 0xFF, 0xFC, 0xFF, 0xFF};
  as[8] = DectoBCD(gettime[0] - 2000);
  as[7] = DectoBCD(y);
  as[5] = DectoBCD(gettime[2]);
  as[4] = DectoBCD(gettime[3]);
  as[3] = DectoBCD(gettime[4]);
  as[2] = DectoBCD(gettime[5]);
  Serial1.write(as, 13);
}
void setday(int y) {
  unsigned char as[13] = {0xEE, 0x81, 0x48, 0x54, 0x17, 0x06, 0x03, 0x07, 0x17, 0xFF, 0xFC, 0xFF, 0xFF};
  as[8] = DectoBCD((gettime[0]) - 2000);
  as[7] = DectoBCD(gettime[1]);
  as[5] = DectoBCD(y);
  as[4] = DectoBCD(gettime[3]);
  as[3] = DectoBCD(gettime[4]);
  as[2] = DectoBCD(gettime[5]);
  Serial1.write(as, 13);
}
void sethour(int y) {
  unsigned char as[13] = {0xEE, 0x81, 0x48, 0x54, 0x17, 0x06, 0x03, 0x07, 0x17, 0xFF, 0xFC, 0xFF, 0xFF};
  as[8] = DectoBCD((gettime[0]) - 2000);
  as[7] = DectoBCD(gettime[1]);
  as[5] = DectoBCD(gettime[2]);
  as[4] = DectoBCD(y);
  as[3] = DectoBCD(gettime[4]);
  as[2] = DectoBCD(gettime[5]);
  Serial1.write(as, 13);
}
void setminute(int y) {
  unsigned char as[13] = {0xEE, 0x81, 0x48, 0x54, 0x17, 0x06, 0x03, 0x07, 0x17, 0xFF, 0xFC, 0xFF, 0xFF};
  as[8] = DectoBCD((gettime[0]) - 2000);
  as[7] = DectoBCD(gettime[1]);
  as[5] = DectoBCD(gettime[2]);
  as[4] = DectoBCD(gettime[3]);
  as[3] = DectoBCD(y);
  as[2] = DectoBCD(gettime[5]);
  Serial1.write(as, 13);
}
void setsecond(int y) {
  unsigned char as[13] = {0xEE, 0x81, 0x48, 0x54, 0x17, 0x06, 0x03, 0x07, 0x17, 0xFF, 0xFC, 0xFF, 0xFF};
  as[8] = DectoBCD((gettime[0]) - 2000);
  as[7] = DectoBCD(gettime[1]);
  as[5] = DectoBCD(gettime[2]);
  as[4] = DectoBCD(gettime[3]);
  as[3] = DectoBCD(gettime[4]);
  as[2] = DectoBCD(y);
  Serial1.write(as, 13);
}
//格式化数据
void settext(uint16 a, uint16 b, float c) {
  unsigned char as[7] = {0xEE, 0xB1, 0x10, 0x00, 0x02, 0x00, 0x0B};
  unsigned char asend[4] = {0xFF, 0xFC, 0xFF, 0xFF};
  unsigned char add[20] = {};
  as[4] = DectoBCD(a);
  as[6] = StrToHex(b);
  for (int s = 0; s < 7; s++) {
    add[s] = as[s];
  }
  for (int s1 = 0; s1 < getDigits(c)[10]; s1++) {
    add[7 + s1] = DectoBCD(getDigits(c)[s1] + 30);
  }
  for (int s2 = 0; s2 < 4; s2++) {
    add[s2 + 7 + getDigits(c)[10]] = asend[s2];
  }
  for (int s3 = 0; s3 < 13; s3++) {
    if (add[s3] == 40) {
      add[s3] = 0x2E;
    }
  }
  Serial1.write(add, 11 + getDigits(c)[10]);
}
void setprintime(int c) {
  unsigned char addx[31] = {0xEE, 0xB1, 0x10, 0x00, 0x00, 0x00, 0x01, 0x32, 0x30, 0x31, 0x37, 0x2D, 0x30, 0x37, 0x2D, 0x30, 0x31, 0x20, 0x30, 0x33, 0x3A, 0x30, 0x33, 0x3A, 0x30, 0x38, 0xFF, 0xFC, 0xFF, 0xFF};
  unsigned int timecc[6];
  unsigned int cca[6];
  unsigned int ccb[2];
  for (char b = 0; b < 6; b++) {
    timecc[b] = gettimes(c)[b];
    pptime[b] = timecc[b];
  }
  //设置年
  for (int qq = 7; qq < 11; qq++) {
    addx[qq] = DectoBCD((getDi(timecc[0])[qq - 7]) + 30);
  }
  //设置月
  if (timecc[1] < 10)
  {
    for (int qq1 = 13; qq1 < 14; qq1++)
    {
      addx[qq1] = DectoBCD((getDi(timecc[1])[qq1 - 13]) + 30);
    }
  }
  else if (timecc[1] >= 10)
  {
    for (int qq1 = 12; qq1 < 14; qq1++)
    {
      addx[qq1] = DectoBCD((getDi(timecc[1])[qq1 - 12]) + 30);
    }
  }
  //设置日
  if (timecc[2] < 10)
  {
    for (int qq2 = 16; qq2 < 17; qq2++)
    {
      addx[qq2] = DectoBCD((getDi(timecc[2])[qq2 - 16]) + 30);
    }
  }
  else if (timecc[2] >= 10)
  {
    for (int qq2 = 15; qq2 < 17; qq2++)
    {
      addx[qq2] = DectoBCD((getDi(timecc[2])[qq2 - 15]) + 30);
    }
  }
  //设置时
  if (timecc[3] < 10)
  {
    for (int qq3 = 19; qq3 < 20; qq3++)
    {
      addx[qq3] = DectoBCD((getDi(timecc[3])[qq3 - 19]) + 30);
    }
  }
  else if (timecc[3] >= 10)
  {
    for (int qq3 = 18; qq3 < 20; qq3++)
    {
      addx[qq3] = DectoBCD((getDi(timecc[3])[qq3 - 18]) + 30);
    }
  }
  //设置分
  if (timecc[4] < 10)
  {
    for (int qq3 = 22; qq3 < 23; qq3++)
    {
      addx[qq3] = DectoBCD((getDi(timecc[4])[qq3 - 22]) + 30);
    }
  }
  else if (timecc[4] >= 10)
  {
    for (int qq3 = 21; qq3 < 23; qq3++)
    {
      addx[qq3] = DectoBCD((getDi(timecc[4])[qq3 - 21]) + 30);
    }
  }
  //设置秒
  if (timecc[5] < 10)
  {
    for (int qq3 = 25; qq3 < 26; qq3++)
    {
      addx[qq3] = DectoBCD((getDi(timecc[5])[qq3 - 25]) + 30);
    }
  }
  else if (timecc[5] >= 10)
  {
    for (int qq3 = 24; qq3 < 26; qq3++)
    {
      addx[qq3] = DectoBCD((getDi(timecc[5])[qq3 - 24]) + 30);
    }
  }
  Serial1.write(addx, 30);
}
//取模1234变1/2/3/4,有小数点
int *getDigits(float num) {
  String i;
  i = String(num);
  for (int ss = 0; ss < int(i.length()); ss++) {
    xxyz[ss] = i[ss] - 48;
  }
  xxyz[10] = i.length(); //发送长度
  return xxyz;
}
//取模1234变1/2/3/4 无小数点
int *getDi(int num) {
  String i;
  i = String(num);
  for (int ss = 0; ss < int(i.length()); ss++) {
    xxyz[ss] = i[ss] - 48;
  }
  xxyz[10] = i.length(); //发送长度
  return xxyz;
}
//转换为十六进制
unsigned int StrToHex(int y) {
  int pk;
  switch (y) {
    case 11: pk = 0x0B; return pk;
    case 12: pk = 0x0C; return pk;
    case 20: pk = 0x14; return pk;
    default: return 0;
  }
}
void spend() {
  static unsigned int d1 = 0, d2 = 0, d3 = 0;
  d1++;
  if (d1 == 3)
  {
    nf1 = hhxx1;
    hhxx1 = 0;
    d2++;
    longs++;
    if (d2 == 2999) {
      d2 = 0;
    }
    oldtime[d2] = nf1;
    d1 = 0;
  }
  nf = hhxx;
  hhxx = 0;
}
void Encodera() {
  hhxx++;
  hhxx1++;
}
void printzd() {
  peizhi++;
}
//测速
float averspeed() {
  float b;
  b = (xdimax * pi) / xplusex * ((nf + nf1) * 5) * 60 / 1000;
  return b;
}
//接激光器串口输出
void setjgq() {
  unsigned char addxx[] = {0x23, 0x53, 0x53, 0x02, 0x33, 0x32, 0x33, 0x30, 0x33, 0x31, 0x33, 0x37, 0x32, 0x3D, 0x33, 0x30, 0x33, 0x37, 0x32, 0x3D, 0x33, 0x30, 0x33, 0x32, 0x32, 0x30, 0x33, 0x30, 0x33, 0x33, 0x33, 0x3A, 0x33, 0x30, 0x33, 0x38, 0x33, 0x3A, 0x33, 0x30, 0x33, 0x35, 0x03};
  //设置年
  addxx[5] = DectoBCD((getDi(pptime[0])[0]) + 30);
  addxx[7] = DectoBCD((getDi(pptime[0])[1]) + 30);
  addxx[9] = DectoBCD((getDi(pptime[0])[2]) + 30);
  addxx[11] = DectoBCD((getDi(pptime[0])[3]) + 30);
  //设置月
  if (pptime[1] < 10) {
    addxx[17] = DectoBCD((getDi(pptime[1])[0]) + 30);
  }
  else if (pptime[1] >= 10) {
    addxx[17] = DectoBCD((getDi(pptime[1])[1]) + 30);
    addxx[15] = DectoBCD((getDi(pptime[1])[0]) + 30);
  }
  //设置日
  if (pptime[2] < 10) {
    addxx[23] = DectoBCD((getDi(pptime[2])[0]) + 30);
  }
  else if (pptime[2] >= 10) {
    addxx[23] = DectoBCD((getDi(pptime[2])[1]) + 30);
    addxx[21] = DectoBCD((getDi(pptime[2])[0]) + 30);
  }

  //设置时
  if (pptime[3] < 10) {
    addxx[29] = DectoBCD((getDi(pptime[3])[0]) + 30);
  }
  else if (pptime[3] >= 10) {
    addxx[29] = DectoBCD((getDi(pptime[3])[1]) + 30);
    addxx[27] = DectoBCD((getDi(pptime[3])[0]) + 30);
  }
  //设置分
  if (pptime[4] < 10) {
    addxx[35] = DectoBCD((getDi(pptime[4])[0]) + 30);
  }
  else if (pptime[4] >= 10) {
    addxx[35] = DectoBCD((getDi(pptime[4])[1]) + 30);
    addxx[33] = DectoBCD((getDi(pptime[4])[0]) + 30);
  }
  //设置秒
  if (pptime[5] < 10) {
    addxx[41] = DectoBCD((getDi(pptime[5])[0]) + 30);
  }
  else if (pptime[5] >= 10) {
    addxx[41] = DectoBCD((getDi(pptime[5])[1]) + 30);
    addxx[39] = DectoBCD((getDi(pptime[5])[0]) + 30);
  }

  //    Serial2.write(addxx,43);
}
//激光器网络输出接口
void setintl() {
  unsigned char addxx[] = { 0x81, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x26, 0x00, 0x32, 0x00, 0x30, 0x00, 0x31, 0x00, 0x37, 0x00, 0x2D, 0x00, 0x30, 0x00, 0x37, 0x00, 0x2D, 0x00, 0x31, 0x00, 0x34, 0x00, 0x20, 0x00, 0x31, 0x00, 0x35, 0x00, 0x3A, 0x00, 0x34, 0x00, 0x35, 0x00, 0x3A, 0x00, 0x32, 0x00, 0x32 };
  static char  pf = 0x01;
  pf++;
  if (pf == 0x09) {
    pf = 0x01;
  }
  addxx[4] = pf;
  //设置年
  addxx[10] = DectoBCD((getDi(pptime[0])[0]) + 30);
  addxx[12] = DectoBCD((getDi(pptime[0])[1]) + 30);
  addxx[14] = DectoBCD((getDi(pptime[0])[2]) + 30);
  addxx[16] = DectoBCD((getDi(pptime[0])[3]) + 30);
  //设置月
  if (pptime[1] < 10) {
    addxx[22] = DectoBCD((getDi(pptime[1])[0]) + 30);
  }
  else if (pptime[1] >= 10) {
    addxx[22] = DectoBCD((getDi(pptime[1])[1]) + 30);
    addxx[20] = DectoBCD((getDi(pptime[1])[0]) + 30);
  }
  //设置日
  if (pptime[2] < 10) {
    addxx[28] = DectoBCD((getDi(pptime[2])[0]) + 30);
  }
  else if (pptime[2] >= 10) {
    addxx[28] = DectoBCD((getDi(pptime[2])[1]) + 30);
    addxx[26] = DectoBCD((getDi(pptime[2])[0]) + 30);
  }

  //设置时
  if (pptime[3] < 10) {
    addxx[34] = DectoBCD((getDi(pptime[3])[0]) + 30);
  }
  else if (pptime[3] >= 10) {
    addxx[34] = DectoBCD((getDi(pptime[3])[1]) + 30);
    addxx[32] = DectoBCD((getDi(pptime[3])[0]) + 30);
  }
  //设置分
  if (pptime[4] < 10) {
    addxx[40] = DectoBCD((getDi(pptime[4])[0]) + 30);
  }
  else if (pptime[4] >= 10) {
    addxx[40] = DectoBCD((getDi(pptime[4])[1]) + 30);
    addxx[38] = DectoBCD((getDi(pptime[4])[0]) + 30);
  }
  //设置秒
  if (pptime[5] < 10) {
    addxx[46] = DectoBCD((getDi(pptime[5])[0]) + 30);
  }
  else if (pptime[5] >= 10) {
    addxx[46] = DectoBCD((getDi(pptime[5])[1]) + 30);
    addxx[44] = DectoBCD((getDi(pptime[5])[0]) + 30);
  }

  Serial2.write(addxx, 47);
}
